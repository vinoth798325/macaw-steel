import { ROUTE_PATHS } from "./routePath";
import Home from "../components/pages/Home";
import About from "../components/pages/About";
import Services from "../components/pages/Services";
import Projects from "../components/pages/Projects";

export const routes = [
    {
        path: ROUTE_PATHS.Home,
        Component: Home,
        exact: true,
    },
    {
        path: ROUTE_PATHS.About,
        Component: About,
        exact: true,
    },
    {
        path: ROUTE_PATHS.Services,
        Component: Services,
        exact: true,
    },
    {
        path: ROUTE_PATHS.Projects,
        Component: Projects,
        exact: true,
    },
]
export const ROUTE_PATHS = {
    Home: "/",
    About: "/about",
    Services: "/services",
    Projects: "/projects",
}
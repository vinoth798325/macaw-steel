import { Component } from "react";

export default class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  render() {
    if (this.state.hasError) {
      //   return this.props.fallback;
      return (
        <img
          src={require("./asset/went-wrong.jpg")}
          alt="Loading..."
          style={{ height: "99.5vh", width: "100vw" }}
        ></img>
      );
    }

    return this.props.children;
  }
}

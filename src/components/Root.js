import { CircularProgress, ThemeProvider, styled } from '@mui/material'
import React, { Suspense } from 'react'
import { Route, Routes } from 'react-router-dom';
import { routes } from '../routes/routes';
import Layout from './shared/Layout';
import theme from '../styles/theme';

const LoaderCircle = styled(CircularProgress)(() => ({
    position: "absolute",
    top: "50vh",
    left: "50%",
    zIndex: "7000",
}));

export const Root = () => {
    return (
        <ThemeProvider theme={theme}>
            <Suspense fallback={<LoaderCircle />}>
                <Routes>
                    <Route path='/' element={<Layout />}>
                        {routes.map(({ path, Component, exact }) => (
                            <Route path={path} element={<Component />} exact={exact} key={path} />
                        ))}
                    </Route>
                </Routes>
            </Suspense>
        </ThemeProvider>
    )
}

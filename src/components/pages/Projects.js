import React from 'react'
import InitialBanner from '../shared/InitialBanner'
import { Box, Grid, Typography, styled } from '@mui/material'
import HeaderText from '../shared/HeaderText'
import ChildContainer from '../shared/ChildContainer';
import { projectsList } from '../constant';
import Footer from './Footer';
import { PAGE_BG_COLOR } from '../../styles/colorConstant';

const ProjectContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    padding: "60px 0",
    backgroundColor: PAGE_BG_COLOR,
    [theme.breakpoints.down("md")]: {
        padding: "40px 0",

    },
}));


const Projects = () => {
    return (
        <>
            <InitialBanner style={{ height: "60vh" }}>
                <Box sx={{ display: "flex", justifyContent: "center", height: "60vh" }}>
                    <Box sx={{ width: "70%", display: "flex", alignItems: "center", height: "100%" }}>
                        <HeaderText textStyle={{ fontSize: { lg: "5vw", xs: "8vw" } }}>Projects</HeaderText>
                    </Box>
                </Box>
            </InitialBanner>

            <ProjectContainer>
                <ChildContainer>
                    <Grid container>
                        {projectsList?.map((val) => (
                            <Grid item lg={4} key={val?.id} pb={8}>
                                <div class="container-p">
                                    <div class="content-p">
                                        <div class="content-overlay-p"></div>
                                        <img class="content-image-p" src={val?.path} alt={val.id} />
                                        <div class="content-details-p fadeIn-top-p fadeIn-left-p">
                                            <Typography>
                                                {val?.description}
                                            </Typography>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                        ))}
                    </Grid>
                </ChildContainer>
            </ProjectContainer>

            <Footer />
        </>
    )
}

export default Projects
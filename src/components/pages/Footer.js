import { Box, Grid, Typography, styled } from '@mui/material'
import React from 'react'
import ChildContainer from '../shared/ChildContainer';
import { PRIMARY_COLOR } from '../../styles/colorConstant';
import PlaceIcon from '@mui/icons-material/Place';
import EmailIcon from '@mui/icons-material/Email';
import CallIcon from '@mui/icons-material/Call';
import { ROUTE_PATHS } from '../../routes/routePath';
import { useNavigate } from 'react-router-dom';

const FooterContainer = styled(Box)(({ theme }) => ({
    backgroundColor: "black",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "white"
}));

const FooterTitle = styled(Typography)(({ theme }) => ({
    fontSize: 24,
    color: PRIMARY_COLOR,
    fontWeight: 600,
    [theme.breakpoints.down("md")]: {
        // textAlign: "center"
    },
}));

const FooterText = styled(Typography)(({ theme }) => ({
    fontSize: 18,
    marginLeft: 12,
    [theme.breakpoints.down("md")]: {
        fontSize: "4vw",
    },
}));

const RowBox = styled(Box)(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    margin: "12px 0"
}));

const QuickLinkText = styled(Typography)(({ theme }) => ({
    fontSize: 18,
    marginBottom: 12,
    cursor: "pointer",
    '&:hover': {
        textDecorationLine: "underline"
    },
}));

const BottomNav = [
    { name: 'Home', path: ROUTE_PATHS?.Home },
    { name: 'About Us', path: ROUTE_PATHS?.About },
    { name: 'Services', path: ROUTE_PATHS?.Services },
    { name: 'Projects', path: ROUTE_PATHS?.Projects },
]

const Footer = () => {
    const navigate = useNavigate();

    const handleScrollClick = (event) => {
        const anchor = (event.target.ownerDocument || document).querySelector(
            "#back-to-top-anchor"
        );

        if (anchor) {
            anchor.scrollIntoView({
                block: "center",
            });
        }
    };

    return (
        <FooterContainer>
            <ChildContainer style={{ margin: "40px 0" }}>
                <Grid container>
                    <Grid item lg={6}>
                        <FooterTitle>Contact Us</FooterTitle>
                        <RowBox>
                            <PlaceIcon color='light' />
                            <FooterText>
                                22/06, Viyasar Street, Ayappa Nagar,
                                Trichy - 620021, TamilNadu, India.
                            </FooterText>
                        </RowBox>
                        <RowBox>
                            <EmailIcon color='light' />
                            <FooterText>info@macawsteel.com</FooterText>
                        </RowBox>
                        <RowBox>
                            <CallIcon color='light' />
                            <Box>
                                <FooterText>India +91-9345997181</FooterText>
                                <FooterText>UAE +971-522230770</FooterText>
                            </Box>
                        </RowBox>

                    </Grid>
                    <Grid item lg={6} sx={{ display: { lg: "flex", xs: "none" }, justifyContent: "space-between" }}>
                        <Box>
                            <FooterTitle style={{ marginBottom: 8 }}>Quick Links</FooterTitle>
                            {BottomNav?.map((val) => (
                                <QuickLinkText onClick={(e) => {
                                    navigate(val?.path)
                                    handleScrollClick(e)
                                }}>{val?.name} </QuickLinkText>
                            ))}
                        </Box>
                        <Box>
                            {/* <FooterTitle style={{ marginBottom: 8 }}>Scan For Details</FooterTitle> */}
                            <img src={require("../../asset/qr-code.png")} alt='QR-Code' style={{ height: "200px", width: "180px", marginTop: 8 }} />
                        </Box>
                    </Grid>
                </Grid>
            </ChildContainer>
        </FooterContainer>
    )
}

export default Footer
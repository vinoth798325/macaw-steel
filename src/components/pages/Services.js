import React from 'react'
import InitialBanner from '../shared/InitialBanner'
import { Box, Grid, Typography, styled } from '@mui/material'
import HeaderText from '../shared/HeaderText'
import ChildContainer from '../shared/ChildContainer';
import HeaderTitle from '../shared/HeaderTitle';
import { PAGE_BG_COLOR } from '../../styles/colorConstant';
import { listOfServices } from '../constant';
import Footer from './Footer';

const ServiceContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    padding: "80px 0",
    backgroundColor: PAGE_BG_COLOR,
    [theme.breakpoints.down("md")]: {
        padding: "40px 0",

    },
}));

const ListText = styled(Typography)(({ theme }) => ({
    marginRight: "8%",
    marginTop: "24px"
}));

const ListBox = styled(Box)(({ theme }) => ({
    backgroundColor: PAGE_BG_COLOR,
    width: "94%",
    height: "100%",
    borderRadius: 20,
    padding: 20,
    boxSizing: "border-box",
    [theme.breakpoints.down("md")]: {
        width: "100%",
    },
}));

const ListGrid = styled(Grid)(({ theme }) => ({
    padding: "1.5% 0",
    [theme.breakpoints.down("md")]: {
        marginBottom: "8%"
    },
}));

const Services = () => {
    return (
        <>
            <InitialBanner style={{ height: "60vh" }}>
                <Box sx={{ display: "flex", justifyContent: "center", height: "60vh" }}>
                    <Box sx={{ width: "70%", display: "flex", alignItems: "center", height: "100%" }}>
                        <HeaderText textStyle={{ fontSize: { lg: "5vw", xs: "8vw" } }} style={{ margin: 0 }}>Services</HeaderText>
                    </Box>
                </Box>
            </InitialBanner>

            {/* our services */}
            <ServiceContainer>
                <ChildContainer>
                    <HeaderTitle style={{ fontSize: { lg: "1.6vw", xs: "4.5vw" }, textAlign: "center", fontWeight: 600 }} >
                        Macaw Steel is a leading provider of structural design and detailing services, delivering innovative solutions for complex projects. Our team of experts, experienced in detailing and modeling, brings technical proficiency and deep industry knowledge to every project. We provide precise, reliable, and timely services, ensuring superior quality and error-free solutions.
                    </HeaderTitle>
                </ChildContainer>
            </ServiceContainer>

            {/* list for services */}
            <ServiceContainer style={{ backgroundColor: "white" }}>
                <ChildContainer>
                    <Grid container>
                        {listOfServices?.map((val) => (
                            <ListGrid item lg={6} key={val?.id}>
                                <ListBox>
                                    <HeaderTitle style={{ fontSize: { lg: "1.3vw", xs: "4.5vw" } }}>{val?.order} </HeaderTitle>
                                    <HeaderText textStyle={{ fontSize: { lg: "1.8vw", xs: "5.5vw" } }} style={{ margin: 0 }} lineStyle={{ width: "100px", marginTop: 1 }}>{val?.title} </HeaderText>
                                    <ListText>{val?.bodyText} </ListText>
                                </ListBox>
                            </ListGrid>
                        ))}
                    </Grid>
                </ChildContainer>
            </ServiceContainer>

            <Footer />
        </>
    )
}

export default Services
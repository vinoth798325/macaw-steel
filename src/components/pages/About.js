import React from 'react'
import InitialBanner from '../shared/InitialBanner'
import { Box, Grid, Typography, styled } from '@mui/material'
import HeaderText from '../shared/HeaderText'
import ChildContainer from '../shared/ChildContainer';
import ImageBack from "../../asset/about-2.webp"
import { makesDifferentValues } from '../constant';
import Footer from './Footer';

const AboutContainer = styled(Box)(({ theme }) => ({
    // backgroundColor: PAGE_BG_COLOR,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
}));

const AboutGrid = styled(Grid)(({ theme }) => ({
    height: "500px",
    [theme.breakpoints.down("md")]: {
        height: "400px",
    },
}));

const MakeContainer = styled(Box)(({ theme }) => ({
    height: "500px",
    backgroundImage: 'url(' + ImageBack + ')',
    backgroundRepeat: "no-repeat",
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    [theme.breakpoints.down("md")]: {
        height: "auto",
    },
}));

const MakeSubContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    justifyContent: "space-evenly",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: "rgba(0, 0, 0, 0.7)",
    width: "100%",
    height: "100%",
    color: "white"
}));

const IconContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
        flexDirection: "column"
    },
}));

const AlignIconContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    // justifyContent: "space-between",
    alignItems: "center",
    width: "20%",
    height: "220px",
    flexDirection: "column",
    [theme.breakpoints.down("md")]: {
        width: "100%"
    },
}));

const IconText = styled(Box)(({ theme }) => ({
    fontSize: "1.2vw",
    fontWeight: 700,
    textAlign: "center",
    [theme.breakpoints.down("md")]: {
        fontSize: "4.5vw",
    },
}));

const IconImg = styled("img")(({ theme }) => ({
    height: "150px",
    width: "150px",
    [theme.breakpoints.down("md")]: {
        height: "120px",
        width: "120px",
    },
}));

const About = () => {
    return (
        <>
            <InitialBanner style={{ height: "60vh" }}>
                <Box sx={{ display: "flex", justifyContent: "center", height: "60vh" }}>
                    <Box sx={{ width: "70%", display: "flex", alignItems: "center", height: "100%" }}>
                        <HeaderText textStyle={{ fontSize: { lg: "5vw", xs: "8vw" } }}>About Us</HeaderText>
                    </Box>

                </Box>
            </InitialBanner>

            {/* About container */}
            <AboutContainer>
                <ChildContainer>
                    <Grid container mt={{ lg: 16, xs: 8 }} mb={{ lg: 16, xs: 8 }}>
                        <AboutGrid item lg={6}>
                            <HeaderText style={{ margin: 0 }}>
                                Our Commitment to Excellence
                            </HeaderText>
                            <Typography mt={{ lg: 6, xs: 4 }} mr={{ lg: 12, xs: 0 }}>
                                Macaw Steel is a trusted name in structural design and detailing, providing accurate and timely solutions to clients with complex, multifaceted projects. Our commitment to excellence is reflected in our technical expertise, extensive experience, and access to a large pool of well-experienced detailers and checkers. At Macaw Steel, we prioritize fostering long-term relationships with clients, providing error-free solutions that contribute to the overall success of their projects.
                            </Typography>
                        </AboutGrid>
                        <AboutGrid item lg={6}>
                            <img src={require("../../asset/about-us.webp")} alt='About Us' style={{ height: "100%", width: "100%" }} />
                        </AboutGrid>
                    </Grid>
                </ChildContainer>
            </AboutContainer>

            {/* Makes us different */}
            <MakeContainer>
                <MakeSubContainer>
                    <HeaderText style={{ margin: { lg: 0, xs: "10% 0" }, color: "white", }}>
                        What Makes Us Different
                    </HeaderText>
                    <ChildContainer >
                        <IconContainer>
                            {
                                makesDifferentValues?.map((val) => (
                                    <AlignIconContainer key={val?.id}>
                                        <IconImg src={val?.icon} alt={val?.title || ""} />
                                        <IconText>{val?.title} </IconText>
                                    </AlignIconContainer>

                                ))
                            }
                        </IconContainer>

                    </ChildContainer>
                </MakeSubContainer>
            </MakeContainer>

            <Footer />
        </>
    )
}

export default About
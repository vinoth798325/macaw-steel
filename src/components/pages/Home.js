import React from 'react'
import InitialBanner from '../shared/InitialBanner'
import { Box, Grid, Paper, Typography, styled } from '@mui/material'
import { PAGE_BG_COLOR, PRIMARY_COLOR } from '../../styles/colorConstant';
import ChildContainer from '../shared/ChildContainer';
import HeaderText from '../shared/HeaderText';
import HeaderTitle from '../shared/HeaderTitle';
import CustomButton from '../shared/CustomButton';
import Footer from './Footer';
import { ROUTE_PATHS } from '../../routes/routePath';
import { useNavigate } from 'react-router-dom';

const BannerText = styled(Typography)(({ theme }) => ({
    fontSize: "5vw",
    fontWeight: 800,
    marginTop: "6%",
    [theme.breakpoints.down("md")]: {
        fontSize: "7vw",
        marginTop: "30%",
    },
}));

const FeatureContainer = styled(Box)(({ theme }) => ({
    backgroundColor: PAGE_BG_COLOR,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
}));

const FeatureGrid = styled(Grid)(({ theme }) => ({
    height: "370px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    boxSizing: "border-box",
}));

const FeatureText = styled(Typography)(({ theme }) => ({
    textAlign: "center",
    margin: 20,
    [theme.breakpoints.down("md")]: {
        fontSize: "4vw",
    },
}));

const MissionGrid = styled(Grid)(({ theme }) => ({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    boxSizing: "border-box",
    padding: "40px 0"
}));

const MissionPaper = styled(Paper)(({ theme }) => ({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    boxSizing: "border-box",
    width: "94%",
    padding: "0 40px",
    borderRadius: "20px",
    backgroundColor: PAGE_BG_COLOR
}));

const Home = () => {
    const navigate = useNavigate()

    const handleScrollClick = (event) => {
        const anchor = (event.target.ownerDocument || document).querySelector(
            "#back-to-top-anchor"
        );

        if (anchor) {
            anchor.scrollIntoView({
                block: "center",
            });
        }
    };

    return (
        <Box>
            <InitialBanner>
                <Box sx={{ display: "flex", justifyContent: "center" }}>
                    <ChildContainer>
                        <BannerText>
                            <span style={{ color: PRIMARY_COLOR }}>Macaw Steel</span> – Precision and Excellence in Steel Detailing
                        </BannerText>
                    </ChildContainer>
                </Box>
            </InitialBanner>

            {/* Our key features */}
            <FeatureContainer>
                <HeaderText>
                    Our Key Features
                </HeaderText>
                <ChildContainer>
                    <Grid container>
                        <FeatureGrid item lg={4}>
                            <img src={require("../../asset/structural.webp")} alt='Structural Design' style={{ height: "100%", width: "100%" }} />
                        </FeatureGrid>
                        <FeatureGrid item lg={4}>
                            <HeaderTitle>
                                Modeling
                            </HeaderTitle>
                            <FeatureText>
                                Our modeling service is designed to help you visualize your project, improving collaboration and reducing errors.
                            </FeatureText>
                        </FeatureGrid>
                        <FeatureGrid item lg={4}>
                            <img src={require("../../asset/autocad.webp")} alt='Autocad' style={{ height: "100%", width: "100%" }} />
                        </FeatureGrid>
                    </Grid>
                    <Grid container>
                        <FeatureGrid item lg={4}>
                            <HeaderTitle>
                                Structural Design
                            </HeaderTitle>
                            <FeatureText>
                                Our structural design solutions are tailored to meet your needs, with a focus on accuracy, stability, and safety.
                            </FeatureText>
                        </FeatureGrid>
                        <FeatureGrid item lg={4}>
                            <img src={require("../../asset/tekla.webp")} alt='Tekla' style={{ height: "100%", width: "100%", }} />
                        </FeatureGrid>
                        <FeatureGrid item lg={4}>
                            <HeaderTitle>
                                 Detailing
                            </HeaderTitle>
                            <FeatureText>
                                Our detailing service ensures that every project is executed to the highest quality standards, with a focus on accuracy and attention to detail.
                            </FeatureText>
                        </FeatureGrid>
                    </Grid>
                </ChildContainer>

                <CustomButton
                    label={"Read More"}
                    color='appColor'
                    style={{ color: "white", margin: 8, marginTop: { lg: 8, xs: 0 } }}
                    onClick={(e) => {
                        navigate(ROUTE_PATHS.Services)
                        handleScrollClick(e)
                    }}
                />

            </FeatureContainer>

            {/* Our Mission */}
            <FeatureContainer style={{ backgroundColor: "white" }}>
                <HeaderText>
                    Why Choose Us
                </HeaderText>
                <ChildContainer>
                    <Grid container>
                        <MissionGrid item lg={6}>
                            <MissionPaper elevation={3}>
                                <HeaderText lineStyle={{ display: "none" }} style={{ margin: 0, marginTop: 2 }} textStyle={{ color: PRIMARY_COLOR }}>24/7</HeaderText>
                                <HeaderTitle>Availability</HeaderTitle>
                                <FeatureText>
                                    We are available round the clock to address your inquiries and provide support for your projects at any time.
                                </FeatureText>
                            </MissionPaper>
                        </MissionGrid>
                        <MissionGrid item lg={6}>
                            <MissionPaper elevation={3}>
                                <HeaderText lineStyle={{ display: "none" }} style={{ margin: 0, marginTop: 2 }} textStyle={{ color: PRIMARY_COLOR }}>99%</HeaderText>
                                <HeaderTitle>Client Satisfaction</HeaderTitle>
                                <FeatureText>
                                    Our commitment to client satisfaction remains at an unmatched level, with 99% of our clients expressing high levels of contentment.
                                </FeatureText>
                            </MissionPaper>
                        </MissionGrid>
                    </Grid>
                </ChildContainer>
            </FeatureContainer>

            {/* Footer */}
            <Footer />
        </Box>
    )
}

export default Home
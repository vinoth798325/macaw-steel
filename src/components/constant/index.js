
export const makesDifferentValues = [
    {
        id: 1,
        icon: require("../../asset/icon1.png"),
        title: "Technical Expertise"
    },
    {
        id: 2,
        icon: require("../../asset/icon2.png"),
        title: "Timely Delivery"
    }, {
        id: 3,
        icon: require("../../asset/icon3.png"),
        title: "Efficient Solutions"
    }, {
        id: 4,
        icon: require("../../asset/icon4.png"),
        title: "Large Pool of Detailers and Checkers"
    }, {
        id: 5,
        icon: require("../../asset/icon5.png"),
        title: "Long-Term Relationships"
    },
]

export const listOfServices = [
    {
        id: 1,
        order: "01",
        title: "Structural Design",
        bodyText: "We offer creative structural design solutions, tailored to your project requirements and constraints. Our team leverages extensive experience and technical expertise to deliver superior structural design services that ensure optimal utilization of resources and safety."
    },
    {
        id: 2,
        order: "02",
        title: "Modeling",
        bodyText: "Our modeling capabilities enable us to deliver accurate and efficient modeling solutions, which ensure that our customers can visualize their projects in 3D. Our team uses innovative modeling techniques to create detailed models that provide a clear understanding of the project and streamline the construction process."
    },
    {
        id: 3,
        order: "03",
        title: "Detailing",
        bodyText: "Our team’s proficiency in detailing enables us to provide timely and accurate detailing solutions. We work closely with our clients to create detailed drawings that capture every aspect of the project, ensuring that nothing is left to chance."
    }, {
        id: 4,
        order: "04",
        title: "Quality Assurance",
        bodyText: "We are committed to delivering the highest quality services to our clients. Our team adheres to a strict quality assurance process, ensuring that our services meet the highest industry standards, with error-free solutions and timely delivery."
    }, {
        id: 5,
        order: "05",
        title: "Expertise",
        bodyText: "Our team of experts has extensive experience in the construction industry, providing us with the necessary expertise to deliver superior services. We stay up-to-date with industry trends, ensuring that our customers receive the latest and most innovative solutions."
    }, {
        id: 6,
        order: "06",
        title: "Customer Satisfaction",
        bodyText: "At Macaw Steel, we prioritize customer satisfaction, striving to exceed our clients’ expectations. We work closely with our clients, providing them with regular updates on their projects and ensuring that all their needs are met. Our commitment to customer satisfaction is evident in the long-term relationships we have built with our clients."
    },
]

export const projectsList = [
    {
        id: 1,
        path: require("../../asset/structural.webp"),
        description: "A short description",
    },
    {
        id: 2,
        path: require("../../asset/about-2.webp"),
        description: "A short description",
    }, {
        id: 3,
        path: require("../../asset/about-us.webp"),
        description: "A short description",
    }, {
        id: 4,
        path: require("../../asset/autocad.webp"),
        description: "A short description",
    }, {
        id: 5,
        path: require("../../asset/tekla.webp"),
        description: "A short description",
    }, {
        id: 6,
        path: require("../../asset/banner-bg.webp"),
        description: "A short description",
    },

]
import styled from '@emotion/styled';
import { Box } from '@mui/material';
import React from 'react'
import NavBar from './Navbar';
import { Outlet } from 'react-router-dom';
import { useWindowHeight } from '@react-hook/window-size';

const MainLayout = styled("div")(({ height }) => ({
    height: height - 64,
}));

const BodyContainer = styled(Box)(() => ({
    width: "100%",
    height: "99.4%",
}));

const Layout = () => {
    const height = useWindowHeight();

    return (
        <MainLayout height={height}>
            <NavBar />
            <BodyContainer>
                <Outlet />
            </BodyContainer>
        </MainLayout>
    )
}

export default Layout
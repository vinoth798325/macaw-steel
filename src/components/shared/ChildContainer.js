import { Box } from '@mui/material'
import React from 'react'

const ChildContainer = ({ style, children }) => {
    return (
        <Box sx={{ width:{lg: "70%" , xs:"90%"}, boxSizing: "border-box", ...style }}>
            {children}
        </Box>
    )
}

export default ChildContainer
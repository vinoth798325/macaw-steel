import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
// import ListItemText from "@mui/material/ListItemText";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import {
    BODY_TEXT,
    PAGE_BG_COLOR,
    PRIMARY_COLOR,
} from "../../styles/colorConstant";
import { ROUTE_PATHS } from "../../routes/routePath";
import { useLocation, useNavigate } from "react-router-dom";
import { Fade, useScrollTrigger } from "@mui/material";
import Fab from "@mui/material/Fab";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";

const drawerWidth = 240;

const navItems = [
    { name: "Home", path: ROUTE_PATHS?.Home },
    { name: "About Us", path: ROUTE_PATHS?.About },
    { name: "Services", path: ROUTE_PATHS?.Services },
    { name: "Projects", path: ROUTE_PATHS?.Projects },
];

function ScrollTop(props) {
    const { children, window } = props;
    const trigger = useScrollTrigger({
        target: window ? window() : undefined,
        disableHysteresis: true,
        threshold: 100,
    });

    const handleClick = (event) => {
        const anchor = (event.target.ownerDocument || document).querySelector(
            "#back-to-top-anchor"
        );

        if (anchor) {
            anchor.scrollIntoView({
                block: "center",
            });
        }
    };

    return (
        <Fade in={trigger}>
            <Box
                onClick={handleClick}
                role="presentation"
                sx={{ position: "fixed", bottom: 16, right: 16, zIndex: 1 }}
            >
                {children}
            </Box>
        </Fade>
    );
}

ScrollTop.propTypes = {
    children: PropTypes.element.isRequired,
    window: PropTypes.func,
};

function Navbar(props) {
    const { window } = props;
    const navigate = useNavigate();
    const { pathname } = useLocation()
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen((prevState) => !prevState);
    };

    const handleScrollClick = (event) => {
        const anchor = (event.target.ownerDocument || document).querySelector(
            "#back-to-top-anchor"
        );

        if (anchor) {
            anchor.scrollIntoView({
                block: "center",
            });
        }
    };

    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: "center", }}>
            <Typography variant="h6" sx={{ py: 2, backgroundColor: PAGE_BG_COLOR, fontStyle: "italic", fontSize: "17px ", }}>
                <span style={{ color: PRIMARY_COLOR, fontSize: "22px ", fontStyle: "normal", fontWeight: 700 }}>
                    Macaw Steel{" "}
                </span>
                Engineering Service & Solutions
            </Typography>
            <Divider sx={{ backgroundColor: PRIMARY_COLOR }} />
            <List>
                {navItems.map((item) => (
                    <ListItem
                        key={item?.name}
                        disablePadding
                        onClick={(event) => {
                            navigate(item?.path)
                            handleScrollClick(event)
                        }}
                        sx={{ color: item?.path === pathname ? PRIMARY_COLOR : BODY_TEXT, fontSize: 44, }}

                    >
                        <ListItemButton sx={{ textAlign: "center", fontSize: 44, textTransform: "uppercase" }}>
                            {/* <ListItemText primary={item?.name} /> */}
                            <Typography sx={{ fontSize: 16, fontWeight: 500, textAlign: "center", width: "100%", margin: "8px 0" }}>{item?.name} </Typography>
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
        </Box>
    );

    const container =
        window !== undefined ? () => window().document.body : undefined;

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar sx={{ backgroundColor: PAGE_BG_COLOR }}>
                <Toolbar>

                    <Box sx={{ display: "flex", justifyContent: "space-between", width: "100%", alignItems: "center" }}>
                        <img
                            src={require("../../asset/logo.png")}
                            alt="Logo"
                            style={{
                                height: 64,
                                width: 150,
                                marginRight: 8,
                            }}
                        />
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            edge="end"
                            onClick={handleDrawerToggle}
                            sx={{ mr: 0, display: { sm: "none" }, color: PRIMARY_COLOR }}
                        >
                            <MenuIcon />
                        </IconButton>

                        <Box sx={{ display: { xs: "none", sm: "block" } }}>
                            {navItems.map((item) => (
                                <Button
                                    key={item?.name}
                                    sx={{ color: item?.path === pathname ? PRIMARY_COLOR : BODY_TEXT, marginLeft: 4 }}
                                    color="appColor"
                                    onClick={(event) => {
                                        navigate(item?.path)
                                        handleScrollClick(event)
                                    }}
                                >
                                    <Box sx={{
                                        borderBottom: item?.path === pathname ? `2px solid ${PRIMARY_COLOR}` : "none"
                                    }}>
                                        {item?.name}
                                    </Box>
                                </Button>
                            ))}
                        </Box>
                    </Box>




                </Toolbar>
            </AppBar>
            <nav>
                <Drawer
                    container={container}
                    variant="temporary"
                    open={mobileOpen}
                    anchor="right"
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, //Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: "block", sm: "none" },
                        "& .MuiDrawer-paper": {
                            boxSizing: "border-box",
                            width: drawerWidth,
                        },
                    }}
                >
                    {drawer}
                </Drawer>
            </nav>
            <Toolbar id="back-to-top-anchor" />

            <ScrollTop {...props}>
                <Fab
                    size="small"
                    aria-label="scroll back to top"
                    sx={{
                        backgroundColor: PRIMARY_COLOR,
                        "&:hover": {
                            backgroundColor: PRIMARY_COLOR,
                            opacity: .9
                        }
                    }}
                >
                    <KeyboardArrowUpIcon sx={{ color: "white" }} />
                </Fab>
            </ScrollTop>
        </React.Fragment>
    );
}

Navbar.propTypes = {
    window: PropTypes.func,
};

export default Navbar;

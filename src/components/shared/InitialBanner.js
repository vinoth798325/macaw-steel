import { Box } from '@mui/material'
import React from 'react'
import ImageBack from "../../asset/banner-bg.webp"



const InitialBanner = ({ children, style }) => {

    return (
        <div style={{ position: "relative", }}>
            <Box sx={{
                height: { lg: "70vh", xs: "60vh" },
                // width: "99vw",
                backgroundColor: "red",
                backgroundImage: 'url(' + ImageBack + ')',
                backgroundRepeat: "no-repeat",
                backgroundSize: 'cover',
                backgroundPosition: 'center center',
                opacity: .5,
                ...style
            }}>
            </Box>
            <Box sx={{ position: "absolute", top: 0, left: 0, width: "100%" }}>
                {children}
            </Box>
        </div>
    )
}

export default InitialBanner
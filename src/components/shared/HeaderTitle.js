import { Typography } from '@mui/material'
import React from 'react'
import { FONT_FAMILY } from '../../styles/colorConstant'

const HeaderTitle = ({ style, children }) => {
    return (
        <Typography
            sx={(theme) => ({
                fontSize: "1.9vw",
                fontFamily: FONT_FAMILY,
                fontWeight: 600,
                opacity: .9,
                [theme.breakpoints.down("sm")]: {
                    fontSize: "4.8vw",
                    ...style,

                },
                ...style,

            })}
        >
            {children}
        </Typography>
    )
}

export default HeaderTitle
import { Box, Typography } from '@mui/material'
import React from 'react'
import { FONT_FAMILY, PRIMARY_COLOR } from '../../styles/colorConstant'


const HeaderText = ({ children, style, textStyle, lineStyle }) => {
    return (
        <Box sx={{ margin: 4, display: "flex", flexDirection: "column", ...style }}>
            <Typography
                sx={(theme) => ({
                    fontSize: "2.6vw",
                    fontFamily: FONT_FAMILY,
                    fontWeight: 800,
                    opacity: 0.9,
                    [theme.breakpoints.down("sm")]: {
                        fontSize: "5.5vw",
                        ...textStyle,
                    },
                    ...textStyle,
                })}
            >
                {children}
            </Typography>

            <Box sx={{ width: "40%", border: "2px solid", borderRadius: "100px", marginTop: 2, borderColor: PRIMARY_COLOR, backgroundColor: PRIMARY_COLOR, ...lineStyle }}></Box>
        </Box>
    )
}

export default HeaderText
import React from 'react'
import { Button } from '@mui/material'

const CustomButton = ({ variant, label, style, color, onClick }) => {
    return (
        <Button
            variant={variant || "contained"}
            sx={{ padding: "12px 40px", ...style }}
            color={color}
            onClick={onClick}
        >
            {label || ""}
        </Button>

    )
}

export default CustomButton
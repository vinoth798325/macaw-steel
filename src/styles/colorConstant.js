export const PRIMARY_COLOR = "#cd2d2d";
export const BODY_TEXT = "#373535";
export const PAGE_BG_COLOR = "#fff7f6";

// #f8faf9
// #fff7f6

export const FONT_FAMILY = "Inter, sans-serif";

// export const secondaryAppColor = "#8663AF";
// export const paginationColor = "#FEF2F587";
// export const appBackgroundColor = "#F4EEF0";
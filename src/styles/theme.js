import { createTheme } from "@mui/material/styles";
import { PRIMARY_COLOR } from "./colorConstant";

const theme = createTheme({
  palette: {
    appColor: {
      main: `${PRIMARY_COLOR}`,
    },
    // secondaryAppColor: {
    //   main: `${secondaryAppColor}`,
    // },
    // paginationColor: {
    //   main: `${paginationColor}`,
    // },
    // appBackgroundColor: {
    //   main: `${appBackgroundColor}`,
    // },
  },
  components: {
    MuiTypography: {
      styleOverrides: {
        root: {
          // wordBreak: "break-all",
          fontFamily: "Inter, sans-serif"
        },
      },
    },
  },
});

export default theme;
